﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityVarious.Utils
{
    /// <summary>
    /// Singleton class to change in the project scenes
    /// </summary>
    public class FlowManager : MonoBehaviour
    {
        #region Protected Variables

        protected static FlowManager instance = null;

        #endregion

        #region Public Methods

        /// <summary>
        /// Get singleton instance of the class
        /// </summary>
        /// <returns></returns>
        public static FlowManager Instance()
        {
            return instance;
        }

        public void GotoInit()
        {
            LoadSceneSingle("Init");
        }

        public void GotoMenu()
        {
            LoadSceneSingle("Menu");
        }

        public void GotoInGame()
        {
            LoadSceneSingle("InGame");
        }

        #endregion

        #region Protected Methods

        protected void Awake()
        {
            // Destroy if exist a instance in play
            if (ExistAInstanceInScene())
            {
                DestroyImmediate(gameObject);
                return;
            }

            CreateInstance();
        }

        protected void Start()
        {
            GotoMenu();
        }

        /// <summary>
        /// Check if there is another instance
        /// </summary>
        /// <returns>True if exist another one</returns>
        protected bool ExistAInstanceInScene()
        {
            FlowManager flowManager = FindObjectOfType<FlowManager>();
            return flowManager != null && flowManager != this;
        }

        /// <summary>
        /// Create the singleton instance
        /// </summary>
        protected void CreateInstance()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        /// <summary>
        /// Reset when the instance is destroyed
        /// </summary>
        protected void OnDestroy()
        {
            instance = null;
        }

        /// <summary>
        /// This function unloads asynchronously a scene passed by parameter
        /// </summary>
        /// <param name="sceneName">The scene to be unloaded</param>
        public void UnloadSceneAsync(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        /// <summary>
        /// This function loads a scene in a single way
        /// </summary>
        /// <param name="sceneName">The name of the scene to be loaded in a single way</param>
        public void LoadSceneSingle(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        /// <summary>
        /// This function loads a scene additively
        /// </summary>
        /// <param name="sceneName">The name of the scene to be loaded additively</param>
        public void LoadSceneAdditive(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

        #endregion
    }
}